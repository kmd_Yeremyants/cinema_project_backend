<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\hall\AddHallRequest;
use App\Http\Requests\API\hall\EditHallRequest;
use App\Http\Requests\API\hall\GetHallRequest;
use App\Http\Requests\API\hall\GetListHallRequest;
use App\Http\Requests\API\hall\DeleteHallRequest;

use App\Models\Hall;
use App\Models\User;

class HallController extends APIController
{
    public function getList(GetListHallRequest $request)
    {
        if ($request->hasExpanders()) {
            $query = Hall::with($request->getExpanders());
        } else {
            $query = Hall::query();
        }

        if (User::isAuthenticated($request)) {
            $user = User::getAuthenticated($request);

            if ($user->hasScope('admin')) {
                // No restrictions for admin.
            } else {
                // No restrictions for admin.
            }
        } else {
            // Retrieve public documents.
//            $query = $query->whereHas('category', function($query) {
//                $query->where('visibility', '1');
//            });
        }

        $query = $this->filter($query, $request);
        $query = $this->sort($query, $request);
        $totalPages = $this->countPages($query, $request);
        $query = $this->paginate($query, $request);

        // build image URL
        // and append parsed JSON Template
        $halls = $query->get();
//        foreach ($documents as $key => $value) {
//            $documents[$key]['imageURL'] = $value->image ? url("images/{$value->image}") : null;
//            if ($request->input('withRulesTree')) {
//                $documents[$key]['rulesTree'] = $value->getTemplateRulesTree();
//            }
//        }

        return $this->respondPaginated($halls, $totalPages);
    }

    public function get(GetHallRequest $request, $id)
    {
        if ($request->hasExpanders()) {
            $query = Hall::with($request->getExpanders());
        } else {
            $query = Hall::query();
        }

        $hall = $query->findOrFail($id);

        $hall->imageURL = $hall->image ? url("images/{$hall->image}") : null;

        return $this->respond($hall);
    }

    public function add(AddHallRequest $request)
    {
        $newHall = [];
        $newHall['color']= $request->input('color');
        $newHall = Hall::create($newHall);

        $newHall['imageURL'] = $newHall['image'] ? url("images/{$newHall['image']}") : null;

        return $this->respondCreated($newHall);
    }

    public function edit(EditHallRequest $request, $id)
    {
        $hall = Hall::findOrFail($id);
        $editedHall = [];
        $editedHall['color'] = $request->input('color');
        $editedHall['size'] = $request->input('size');
        $hall->update($editedHall);

        return $this->respondAccepted();
    }

    public function delete(DeleteHallRequest $request, $id)
    {
        $hall = Hall::findOrFail($id);
        $hall->delete();

        return $this->respondAccepted();
    }

}
