<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\movies\AddMovieRequest;
use App\Http\Requests\API\movies\DeleteMovieRequest;
use App\Http\Requests\API\movies\EditMovieRequest;
use App\Http\Requests\API\movies\GetListMoviesRequest;
use App\Http\Requests\API\movies\GetMovieRequest;
use App\Models\Movies;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MoviesController extends APIController
{
    public function getList(GetListMoviesRequest $request)
    {
        if ($request->hasExpanders()) {
            $query = Movies::with($request->getExpanders());
        } else {
            $query = Movies::query();
        }

        if (User::isAuthenticated($request)) {
            $user = User::getAuthenticated($request);

            if ($user->hasScope('admin')) {
                // No restrictions for admin.
            } else {
                // No restrictions for admin.
            }
        } else {
            // Retrieve public documents.
//            $query = $query->whereHas('category', function($query) {
//                $query->where('visibility', '1');
//            });
        }

        $query = $this->filter($query, $request);
        $query = $this->sort($query, $request);
        $totalPages = $this->countPages($query, $request);
        $query = $this->paginate($query, $request);

        // build image URL
        // and append parsed JSON Template
        $halls = $query->get();
//        foreach ($documents as $key => $value) {
//            $documents[$key]['imageURL'] = $value->image ? url("images/{$value->image}") : null;
//            if ($request->input('withRulesTree')) {
//                $documents[$key]['rulesTree'] = $value->getTemplateRulesTree();
//            }
//        }

        return $this->respondPaginated($halls, $totalPages);
    }

    public function get(GetMovieRequest $request, $id)
    {
        if ($request->hasExpanders()) {
            $query = Movies::with($request->getExpanders());
        } else {
            $query = Movies::query();
        }

        $movie = $query->findOrFail($id);

        $movie->imageURL = $movie->image ? url("images/{$movie->image}") : null;

        return $this->respond($movie);
    }


    public function add(AddMovieRequest $request)
    {
        $newMovie = [];
        $newMovie['hall_id']= $request->input('hall_id');
        $newMovie['start_time']= $request->input('start_time');
        $newMovie['duration']= $request->input('duration');
        $newMovie['date']= $request->input('date');
        $newMovie['name']= $request->input('name');
        $newMovie['price']= $request->input('price');
        $newMovie = Movies::create($newMovie);

        $newHall['imageURL'] = $newMovie['image'] ? url("images/{$newMovie['image']}") : null;

        return $this->respondCreated($newHall);
    }

    public function edit(EditMovieRequest $request, $id)
    {
        $movie = Movies::findOrFail($id);
        $editedMovie = [];
        $editedMovie['hall_id'] = $request->input('hall_id');
        $editedMovie['start_time'] = $request->input('start_time');
        $editedMovie['duration'] = $request->input('duration');
        $editedMovie['date'] = $request->input('date');
        $editedMovie['name'] = $request->input('name');
        $editedMovie['price'] = $request->input('price');

        $movie->update($editedMovie);

        return $this->respondAccepted();
    }

    public function delete(DeleteMovieRequest $request, $id)
    {
        $movie = Movies::findOrFail($id);
        $movie->delete();

        return $this->respondAccepted();
    }

}
