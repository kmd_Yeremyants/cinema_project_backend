<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Authorization');
header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
header('Access-Control-Expose-Headers: X-Total-Pages');
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| This route group applies for the API.
|
*/
Route::group(['prefix' => 'api'], function () {
    Route::get('images/{filename}', 'API\StorageController@get');

    Route::get('halls', 'API\HallController@getList');
    Route::get('halls/{id}', 'API\HallController@get');

    Route::get('movies', 'API\MoviesController@getList');
    Route::get('movies/{id}', 'API\MoviesController@get');

    /**
     * Authentication routes
     */
    Route::group(['prefix' => 'auth'], function() {
        Route::post('login',         'API\UsersController@login');
        Route::post('logout',        'API\UsersController@logout');
        Route::post('register',      'API\UsersController@register');
        Route::get('refresh-token',  'API\UsersController@refreshToken');
        Route::get('getUser',        'API\UsersController@getUser');
    });

    /**
     * Auth User Routes
     */
    Route::group(['middleware' => 'api.auth'], function() {
        Route::put('user',                       'API\UsersController@updateUser');
        Route::put('user/updatePassword',        'API\UpdatePasswordController@updatePassword');
        Route::post('user/image',                'API\UsersController@uploadImage');
        Route::put('userRequiredOptionsUpdate',        'API\UsersController@userRequiredOptionsUpdate');
    });
    /**
     * Authenticated routes
     */
    Route::group(['middleware' => ['jwt.auth']], function() {
        Route::get('protected', 'API\UsersController@getProtected');

        /**
         * Admin routes
         */
        Route::group(['middleware' => 'api.admin'], function() {

            Route::put('halls/{id}', 'API\HallController@edit');
            Route::post('halls', 'API\HallController@add');
            Route::delete('halls/{id}', 'API\HallController@delete');

            Route::post('movies','API\MoviesController@add');
            Route::put('movies/{id}','API\MoviesController@edit');
            Route::delete('movies/{id}','API\MoviesController@delete');

        });
    });
});
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
