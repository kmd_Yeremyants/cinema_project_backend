<?php

namespace App\Http\Middleware\API;

use App\Models\User;
use Closure;

class Auth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::getAuthenticated($request);

        if (empty($user)) {
            throw new APIException('unauthorized', HttpResponse::HTTP_UNAUTHORIZED);
        }
        return $next($request);
    }
}
