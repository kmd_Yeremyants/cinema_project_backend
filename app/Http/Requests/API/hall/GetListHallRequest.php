<?php

namespace App\Http\Requests\API\hall;

use App\Http\Requests\API\APIRequest;
use Illuminate\Foundation\Http\FormRequest;

class GetListHallRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Get the entities this request can expand
     *
     * @return array
     */
    public function canExpand()
    {
        return [
            //
        ];
    }
}
