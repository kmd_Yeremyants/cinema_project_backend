<?php

namespace App\Http\Requests\API\users;

use App\Http\Requests\API\APIRequest;
use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;
class GetUserRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the entities this request can expand
     *
     * @return array
     */
    public function canExpand()
    {
        return [
            'downloads',
            'scopes',
            'groups',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
