<?php

namespace App\Http\Requests\API\users;

use App\Http\Requests\API\APIRequest;
use App\Models\User;
class AddUserRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|min:3',
            'last_name' => 'required|string|min:3',
            'email' => 'required|email',
            'password' => 'required|string|min:3|max:12',
        ];
    }
}
