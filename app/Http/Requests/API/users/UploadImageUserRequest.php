<?php

namespace App\Http\Requests\API\users;

use App\Http\Requests\API\APIRequest;


class UploadImageUserRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'image|max:4096',
        ];
    }
}
