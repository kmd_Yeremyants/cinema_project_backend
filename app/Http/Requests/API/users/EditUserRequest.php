<?php

namespace App\Http\Requests\API\users;

use App\Http\Requests\API\APIRequest;
use App\Models\User;
class EditUserRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'=>'string|min:3|required',
            'last_name'=>'string|min:3|required',
            'email'=>'email|required'
        ];
    }
}
