<?php

namespace App\Http\Requests\API\users;

use App\Http\Requests\API\APIRequest;

class GetListUserRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the entities this request can expand
     * 
     * @return array
     */
    public function canExpand()
    {
        return [
            'downloads',
            'scopes',
            'blockedUser',
            'groups'
        ];
    }

    /**
     * Get columns this request can sort
     * 
     * @return array
     */
    public function canSort()
    {
        return [
            //
        ];
    }

    /**
     * Get columns this request can filter
     * 
     * @return array
     */
    public function canFilter()
    {
        return [
            'first_name',
            'last_name',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
