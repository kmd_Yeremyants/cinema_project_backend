<?php

namespace App\Http\Requests\API\users;

use App\Http\Requests\API\APIRequest;

class UpdatePasswordRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }



    /**
     *
     * @return array
     */

    public function rules()
    {
        return [
            'password' => 'required|min:6',
            'newPassword' => 'required|min:6|different:password',
            'confirmPassword' => 'required|min:6|same:newPassword',
        ];
    }
}
