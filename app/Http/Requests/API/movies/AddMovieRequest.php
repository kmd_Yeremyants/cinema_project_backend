<?php

namespace App\Http\Requests\API\movies;

use App\Http\Requests\API\APIRequest;

class AddMovieRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hall_id' => 'required|numeric|exists:halls,id',
            'name' => 'required|max:255',
            'start_time' => 'required',
            'price' => 'numeric|between:0,999.99',
            'duration' => 'required',
            'date' => 'required',
        ];
    }
}
