<?php

namespace App\Http\Requests\API\movies;

use App\Http\Requests\API\APIRequest;

class GetListMoviesRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Get the entities this request can expand
     *
     * @return array
     */
    public function canExpand()
    {
        return [
            'halls'
        ];
    }
}
