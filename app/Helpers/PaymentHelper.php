<?php

namespace App\Helpers;

use App\Models\Document;
use App\Models\User;
use App\Models\Payment;
use Mollie\Laravel\Facades\Mollie;

class PaymentHelper
{
    public static function createPayment(User $user,Document $document)
    {
        $customer = Mollie::api()->customers()->create([
            "name"  => $user->first_name.' '.$user->last_name,
            "email" => $user->email,
            "metadata"=>[
                'user_id'=>$user->id
            ]
        ]);
        Payment::create([
            'customer_id'=>$customer->id,
            'user_id'=>$user->id
        ]);
        $customerId = $customer->id;
//        dd($document->category->description);
        $payment = Mollie::api()->payments()->create([
            'amount'        => $document->price,
            'customerId'    => $customerId,
            'recurringType' => 'first',
            'description'   => $document->category->description,
            'redirectUrl'   => url('/check').'/'.$customerId,
        ]);
        Payment::where('customer_id',$customer->id)
            ->update(['payment_id'=>$payment->id]);
        return redirect($payment->links->paymentUrl);
    }
    public static function checkPayment($request,$id)
    {
        $dbPayment = Payment::where('customer_id',$id)->first();
        $customer = Mollie::api()->customers()->get($dbPayment->customer_id);
        $payment = Mollie::api()->payments()->get($dbPayment->payment_id);
        if ($payment->isPaid()) {
            Payment::find($dbPayment->id)->update([
                'mandate_id'=>$payment->mandateId,
                'amount'=>$payment->amount,
                'status'=>$payment->status,
                'mode'=>$payment->mode,
                'method'=>$payment->method,
                'created_date_time'=>$payment->createdDatetime,
                'paid_datetime'=>$payment->paidDatetime,
            ]);
        }else{
            Payment::find($dbPayment->id)->update([
                'amount'=>$payment->amount,
                'status'=>$payment->status,
                'mode'=>$payment->mode
            ]);
        }
    }
}