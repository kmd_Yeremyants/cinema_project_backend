<?php

namespace App\Models;

use App\Models\Model;

class Movies extends Model
{
    protected $table = "movies";

    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'hall_id',
        'start_time',
        'duration',
        'date',
        'price',
        'path',
        'name',
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    const CREATED_AT = 'created_at';

    const UPDATED_AT = 'updated_at';

    public function movies()
    {
        return $this->belongsTo('App\Models\Hall');
    }
}
