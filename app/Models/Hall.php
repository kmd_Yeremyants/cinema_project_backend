<?php

namespace App\Models;

use App\Models\Model;

class Hall extends Model
{
    protected $table = "halls";

    protected $primaryKey = 'id';

    protected $fillable =[
        'id',
        'color',
        'size',
        'deleted_at',
        'created_at',
        'updated_at'
    ];


    const CREATED_AT = 'created_at';

    const UPDATED_AT = 'updated_at';

    public function movies()
    {
        return $this->hasMany('App\Models\Movies');
    }
}
