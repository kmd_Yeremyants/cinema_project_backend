<?php

namespace App\Models;

use App\Models\Model;
use App\Exceptions\APIException;

/**
 * Class Scope
 * @package App\Models\
 * @property int    $id
 * @property int    $type
 * @property string $name deprecated
 * @property date   $deleted_at
 * @property date   $created_at
 * @property date   $updated_at
 * relations
 * @property user        $users
 */

class Scope extends Model
{
    protected $table = "scopes";

    protected $primaryKey = 'id';

    protected $fillable =[
        'id',
        'type',
        'name',
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    protected $relations =[
        'users'
    ];

    const CREATED_AT = 'created_at';

    const UPDATED_AT = 'updated_at';

    /**
     * The scopes that belong to the user.
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'users_scopes');
    }
    /**
     * Auhtorize a scope get request
     *
     * @param   Request $request
     * @return  boolean
     */
    public static function authorizeGet($request) {
        $scope = self::findOrFail($request->route('id'));
        return ($scope->type == 1);
    }

    /**
     * Auhtorize a scope edit request
     *
     * @param   Request $request
     * @return  bool
     */
    public static function authorizeEdit($request) {
        $scope = self::findOrFail($request->route('id'));
        return ($scope->type == 1);
    }
}
